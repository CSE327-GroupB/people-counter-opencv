# pull ubuntu image
FROM ubuntu:16.04

# install python3
RUN apt-get update
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y

# install dependencies for openCV
RUN apt-get install build-essential cmake -y
RUN apt-get install libopenblas-dev liblapack-dev -y 
RUN apt-get install libx11-dev libgtk-3-dev -y

# install openCV
RUN apt-get update
RUN pip3 install opencv-contrib-python

# install project dependencies
RUN pip3 install imutils
RUN pip3 install scipy
RUN pip3 install numpy
RUN pip3 install dlib

# copy the project folder to the container
COPY people_counter /usr/src/project
