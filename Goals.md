##Our Goal Next Week

As we are following agile process, our first approach was to develop a working demo without worrying about any design pattern.
So, next week, our target will be refactoring the code while following object oriented programming.


##Task list

+ Refactor the code.
+ Develop a Graphical User Interface.
+ Keeping count of people inside the transit vehicle.



