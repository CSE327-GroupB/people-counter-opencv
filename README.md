# People-Counter-openCV

Our final project uses a combination of object detection and object tracking.
Previous approaches had some drawbacks like less accuracy. 
It couldn't distinguish between people and other moving object. 
To solve this problem we used deep learning based object detection. 
In this case we used a pre-trained MobileNet Single Shot Detector model.
Now the program can identify people from other moving objects.

And for object tracking we used centroid tracking algorithm.

Final project files are inside the 'people_counter' folder.

##Usage
    cd people_counter
    python people_counter.py --input videos/example_02.mp4




