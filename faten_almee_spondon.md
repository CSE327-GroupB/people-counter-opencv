As our project is mainly about the detection and counting of the passengers, I tried to detect it first. 
Firstly, I tried to define a minimum area, found contours, for each contour I got the area and if it’s more than, the threshold did something. 
I applied morphological transformation using open-CV and python and lastly, it was able to detect the passengers.
I tried to count the passengers after.
To do this, I first created two imaginary lines that indicates when to evaluate the object’s direction.
I also use two methods on the Person class, going up and going down. If the counter is incremented it counts people. 
Also, the Person class has a State attribute which is used to know when the object is outside the counting limits of the image and release allocated memory.
The file is given on Bitbucket named “people_counter” 
I tried to complete the task. 
Learning object detection and counting process was a new thing for me. 
Our next challenge will be to detect gender and to track the number of passenger inside the bus.
