"""Motion Detection Using BackGround Subtractor (KNN)"""

import cv2 as cv
import imutils


def testIntersectionIn(x, y):
    res = -450 * x + 400 * y + 157500
    if((res >= -550) and  (res < 550)):
        print (str(res))
        return True
    return False

def testIntersectionOut(x, y):
    res = -450 * x + 400 * y + 180000
    if ((res >= -550) and (res <= 550)):
        print (str(res))
        return True

    return False

textIn = 0
textOut = 0

bs = cv.createBackgroundSubtractorKNN(detectShadows = True)
camera = cv.VideoCapture('vtest.avi')


while camera.isOpened():
    ret, frame = camera.read()
    height, width = frame.shape[:2]

    frame = imutils.resize(frame, width = width)
    fgmask = bs.apply(frame)

    th = cv.threshold(fgmask.copy(), 244, 255, cv.THRESH_BINARY)[1]
    dilated = cv.dilate(th, cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3)), iterations = 2)
    image, contours, hierarchy = cv.findContours(dilated, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    for c in contours:
        if cv.contourArea(c) > 800:
            (x,y,w,h) = cv.boundingRect(c)
            cv.rectangle(frame, (x,y), (x+w, y+h), (255, 255, 0), 2)

            cv.line(frame, (548, 398), (724, 261), (250,0,1), 2)
            cv.line(frame, (516, 388), (700, 235), (0, 0, 255), 2)
            rectagleCenterPont = ((x + x + w) /2, (y + y + h) /2)
            cv.circle(frame, rectagleCenterPont, 1, (0, 0, 255), 5)

            if(testIntersectionIn((x + x + w) / 2, (y + y + h) / 2)):
                textIn += 1

            if(testIntersectionOut((x + x + w) / 2, (y + y + h) / 2)):
                textOut += 1

    #cv.imshow("mog", fgmask)
    #cv.imshow("thresh", th)
    #cv.imshow("detection", frame)
    if cv.waitKey(30) & 0xFF == ord('q'):
        break


    cv.putText(frame, "In: {}".format(str(textIn)), (10, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv.putText(frame, "Out: {}".format(str(textOut)), (10, 70),cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv.imshow("Counter", frame)

camera.release()
cv.destroyAllWindows()
