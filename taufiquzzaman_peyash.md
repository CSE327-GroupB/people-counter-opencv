My first approach was to count people with object tracking. Primarily I used KNN algorithm. 
But it had some drawbacks and less accuracy as it counts every moving object.
Finally I tried a hybrid approach, using both the object detection and object tracking. 
I used deep learning based object detection method, namely MobileNet Single Shot Detector(SSD). 
So now the program can detect people and distinguish between people with other object. 
It is now more accurate.
